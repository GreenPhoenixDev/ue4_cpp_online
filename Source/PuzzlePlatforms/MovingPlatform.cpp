// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"

#include "DrawDebugHelpers.h"
#include "Math/Vector.h"

AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}

	GlobalStartingLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(LocalTargetLocation);
	TravelDistance = FVector::Dist(GlobalStartingLocation, GlobalTargetLocation);
	Direction = (GlobalTargetLocation - GetActorLocation()).GetSafeNormal();

	if (RequiredTriggers > 0) { this->SetActorTickEnabled(false); }
}

void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		FVector Location = GetActorLocation();

		if (bMovingForward)
		{
			if (FVector::Dist(GlobalStartingLocation, Location) >= TravelDistance)
			{
				Direction *= -1.f;
				bMovingForward = false;
			}
		}
		else
		{
			if (FVector::Dist(Location, GlobalTargetLocation) >= TravelDistance)
			{
				Direction *= -1.f;
				bMovingForward = true;
			}
		}

		Location += Direction * Speed * DeltaTime;
		SetActorLocation(Location);
	}
}

void AMovingPlatform::AddActiveTrigger()
{
	ActiveTriggers++;
	this->SetActorTickEnabled(true);
}

void AMovingPlatform::RemoveActiveTrigger() 
{
	ActiveTriggers--;
	if (ActiveTriggers <= RequiredTriggers) { this->SetActorTickEnabled(false); }
}